import frequencyCounter.StringFrequencyCounter;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MostFrequentCookieFinder {
    private StringFrequencyCounter frequencyCounter;

    public MostFrequentCookieFinder(StringFrequencyCounter frequencyCounter) {
        this.frequencyCounter = frequencyCounter;
    }

    public List<String> findMostFrequent(List<String> cookies) {
        if (cookies.isEmpty()) {
            return Collections.emptyList();
        }

        Map<String, Integer> cookiesFrequency = frequencyCounter.count(cookies);

        int highestFrequency = (Collections.max(cookiesFrequency.values()));

        return cookiesFrequency.entrySet()
                .stream()
                .filter(entry -> entry.getValue() == highestFrequency)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
