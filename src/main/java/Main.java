import frequencyCounter.StringFrequencyCounter;
import input.InputProcessor;
import input.fileReader.FileReader;
import validations.DateValidator;
import validations.FileValidator;
import validations.exceptions.InvalidFileExtensionException;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Stream;

public class Main {

    private static final String COMMA = ",";
    private static final String CSV_LINE_REGEX = "\\w{16},%sT\\d{2}:\\d{2}:\\d{2}\\+\\d{2}:\\d{2}";

    public static void main(String[] args) throws FileNotFoundException, ParseException, InvalidFileExtensionException {
        String filePath = args[0];
        FileValidator.validate(filePath);

        String date = args[1];
        DateValidator.validateDate(date);

        List<String> mostFrequentCookies = calculateMostFrequentCookie(date, filePath);

        mostFrequentCookies.forEach(System.out::println);
    }

    private static List<String> calculateMostFrequentCookie(String date, String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        FileReader reader = new FileReader();
        Stream<String> fileContent = reader.toStream(file);

        String targetDateRegex = String.format(CSV_LINE_REGEX, date);
        List<String> targetDateLines = InputProcessor.getTargetRecordsByRegex(targetDateRegex, COMMA, fileContent);

        MostFrequentCookieFinder mostFrequentCookieFinder = new MostFrequentCookieFinder(new StringFrequencyCounter());

        return mostFrequentCookieFinder.findMostFrequent(targetDateLines);
    }
}
