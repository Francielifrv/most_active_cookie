package frequencyCounter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StringFrequencyCounter {
    public Map<String, Integer> count(List<String> strings) {
        return strings.stream()
                .collect(Collectors
                        .toMap(string -> string, cookie -> 1, Integer::sum));
    }
}
