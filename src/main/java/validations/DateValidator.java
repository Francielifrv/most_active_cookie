package validations;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateValidator {
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    public static void validateDate(String date) throws ParseException {
        if (date == null) {
            throw new ParseException("Date cannot be null", 0);
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        dateFormat.setLenient(false);
        dateFormat.parse(date);
    }
}
