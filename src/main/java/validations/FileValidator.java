package validations;

import validations.exceptions.InvalidFileExtensionException;

public class FileValidator {
    private static final String CSV_EXETENSION = "csv";

    public static void validate(String filePath) throws InvalidFileExtensionException {
        if (! filePath.endsWith(CSV_EXETENSION)) {
            throw new InvalidFileExtensionException("File extension is not suported. Make sure input file is a csv.");
        }
    }
}
