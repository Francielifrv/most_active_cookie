package input;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputProcessor {

    public static List<String> getTargetRecordsByRegex(String regex, String delimiter, Stream<String> stream) {
        return stream
                .filter(line -> line.matches(regex))
                .map(line -> line.split(delimiter)[0])
                .collect(Collectors.toList());
    }
}
