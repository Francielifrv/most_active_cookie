package input.fileReader;

import java.io.*;
import java.util.stream.Stream;

public class FileReader {
    public Stream<String> toStream(File file) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(file.getAbsoluteFile());
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        return reader.lines().skip(1);
    }
}
