package input.fileReader;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class FileReaderTest {
    private final String CSV_READER = "a_header,another_header";
    private final String A_ROW = "a_csv_row,another_csv_row";

    private FileReader reader;

    @Before
    public void setUp() {
        reader = new FileReader();
    }

    @Test
    public void returnsFileStreamWithoutHeader() throws IOException {
        File fakeCsv = createInMemoryCsvFile(Arrays.asList(CSV_READER, A_ROW));

        Stream<String> streamContent = reader.toStream(fakeCsv);

        List<String> streamAsList = streamContent.collect(Collectors.toList());

        assertThat(streamAsList, hasSize(1));
        assertThat(streamAsList.get(0), is("a_csv_row,another_csv_row"));
    }

    @Test(expected = FileNotFoundException.class)
    public void throwsFileNotFoundExceptionWhenFileDoesNotExist() throws FileNotFoundException {
        File file = new File("a/invalid/file/path/file.csv");

        reader.toStream(file);
    }

    private File createInMemoryCsvFile(List<String> fileContent) throws IOException {
        File file = File.createTempFile("test_file", "csv");
        file.deleteOnExit();

        Files.write(file.toPath(), fileContent, StandardCharsets.UTF_8);

        return file;
    }
}