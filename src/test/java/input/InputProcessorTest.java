package input;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class InputProcessorTest {
    private final String TARGET_DATE_REGEX = "\\w{16},2018-12-09T\\d{2}:\\d{2}:\\d{2}\\+\\d{2}:\\d{2}";
    private final String COMMA_DELIMITER = ",";

    @Test
    public void returnsSingKesMatchingRegexOnStream() {
        List<String> entry = Arrays.asList("AtY0laUfhglK3lC7,2018-12-09T14:19:00+00:00",
                "SAZuXPGUrfbcn5UA,2018-09-09T10:13:00+00:00");

        Stream<String> stream = entry.stream();

        List<String> result = InputProcessor.getTargetRecordsByRegex(TARGET_DATE_REGEX, COMMA_DELIMITER, stream);

        assertThat(result, hasSize(1));
        assertThat(result.get(0), is("AtY0laUfhglK3lC7"));
    }

    @Test
    public void returnsMultipleKeysMatchingRegexOnStream() {
        List<String> entry = Arrays.asList("AtY0laUfhglK3lC7,2018-12-09T14:19:00+00:00",
                "SAZuXPGUrfbcn5UA,2018-09-15T10:13:00+00:00",
                "4sMM2LxV07bPJzwf,2018-12-09T21:30:00+00:00",
                "AtY0laUfhglK3lC7,2018-12-09T14:19:00+00:00");

        Stream<String> stream = entry.stream();

        List<String> result = InputProcessor.getTargetRecordsByRegex(TARGET_DATE_REGEX, COMMA_DELIMITER, stream);

        assertThat(result, hasSize(3));
        assertThat(result, contains("AtY0laUfhglK3lC7", "4sMM2LxV07bPJzwf", "AtY0laUfhglK3lC7"));
    }
}