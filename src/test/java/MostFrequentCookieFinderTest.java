import frequencyCounter.StringFrequencyCounter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MostFrequentCookieFinderTest {
    @Mock
    private StringFrequencyCounter frequencyCounter;

    private MostFrequentCookieFinder mostFrequentCookieFinder;


    @Before
    public void setUp() {
        initMocks(this);
        mostFrequentCookieFinder = new MostFrequentCookieFinder(frequencyCounter);

        Map<String, Integer> cookieFrequency = Map.of("hello", 3, "world", 1, "hey", 4);

        when(frequencyCounter.count(anyList())).thenCallRealMethod();
    }

    @Test
    public void returnsEmptyListWhenCookieListIsEmpty() {
        List<String> cookies = Collections.emptyList();

        List<String> mostFrequentCookie = mostFrequentCookieFinder.findMostFrequent(cookies);

        assertThat(mostFrequentCookie, is(empty()));
    }

    @Test
    public void returnsInputCookieListWhenThereIsOnlyOneCookieOnIt() {
        List<String> cookies = Collections.singletonList("singleCookie");

        List<String> mostFrequentCookie = mostFrequentCookieFinder.findMostFrequent(cookies);

        assertThat(mostFrequentCookie, hasSize(1));
        assertThat(mostFrequentCookie, hasItem("singleCookie"));
    }

    @Test
    public void countsCookieStringFrequency() {
        List<String> cookies = Collections.singletonList("singleCookie");
        when(frequencyCounter.count(anyList())).thenReturn(Map.of("singleCookie", 1));

        mostFrequentCookieFinder.findMostFrequent(cookies);

        verify(frequencyCounter).count(cookies);
    }

    @Test
    public void returnsMostFrequentCookie() {
        List<String> cookies = Arrays.asList("hello", "hello", "hey", "hello", "hey", "hey", "hey", "world");

        List<String> mostFrequentCookie = mostFrequentCookieFinder.findMostFrequent(cookies);

        assertThat(mostFrequentCookie, hasSize(1));
        assertThat(mostFrequentCookie.get(0), is("hey"));
    }

    @Test
    public void returnsMultipleMostFrequentCookie() {
        List<String> cookies = Arrays.asList("hello", "hello", "hello", "hey", "hey", "hey", "world");

        List<String> mostFrequentCookie = mostFrequentCookieFinder.findMostFrequent(cookies);

        assertThat(mostFrequentCookie, hasSize(2));
        assertThat(mostFrequentCookie, hasItems("hello", "hey"));
    }
}