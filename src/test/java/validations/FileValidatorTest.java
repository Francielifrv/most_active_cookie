package validations;

import org.junit.Test;
import validations.exceptions.InvalidFileExtensionException;

import static org.junit.Assert.*;

public class FileValidatorTest {
    @Test
    public void csvFileIsValid() throws InvalidFileExtensionException {
        FileValidator.validate("file.csv");
    }

    @Test(expected = InvalidFileExtensionException.class)
    public void nonCsvFilesAreInvalid() throws InvalidFileExtensionException {
        FileValidator.validate("invalid_file.txt");
    }
}