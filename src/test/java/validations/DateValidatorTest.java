package validations;

import org.junit.Test;

import java.text.ParseException;

public class DateValidatorTest {
    @Test
    public void doesNotThrowExceptionWhenDateHasValidFormat() throws ParseException {
        DateValidator.validateDate("2019-12-01");
    }

    @Test(expected = ParseException.class)
    public void throwsInvalidDateExceptionWhenStringDateIsNull() throws ParseException {
        DateValidator.validateDate(null);
    }

    @Test(expected = ParseException.class)
    public void throwsInvalidDateExceptionWhenMonthIsInvalid() throws ParseException {
        DateValidator.validateDate("2019-13-01");
    }

    @Test(expected = ParseException.class)
    public void throwsInvalidDateExceptionWhenDayIsInvalid() throws ParseException {
        DateValidator.validateDate("2019-01-54");
    }


    @Test(expected = ParseException.class)
    public void throwsInvalidDateExceptionWhenMonthAndDayIsInvalid() throws ParseException {
        DateValidator.validateDate("2019-13-45");
    }
}