package frequencyCounter;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.aMapWithSize;
import static org.hamcrest.Matchers.anEmptyMap;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class StringFrequencyCounterTest {
    StringFrequencyCounter frequencyCounter;

    @Before
    public void setUp() {
        frequencyCounter = new StringFrequencyCounter();
    }

    @Test
    public void returnsEmptyMapWhenStringListIsEmpty() {
        List<String> strings = Collections.emptyList();

        StringFrequencyCounter frequencyCounter = new StringFrequencyCounter();

        Map<String, Integer> result = frequencyCounter.count(strings);

        assertThat(result, is(anEmptyMap()));
    }

    @Test
    public void countsOnlyOneStringOnList() {
        List<String> strings = Collections.singletonList("singleString");

        Map<String, Integer> results = frequencyCounter.count(strings);

        assertThat(results, aMapWithSize(1));
        assertThat(results.get("singleString"), is(1));
    }

    @Test
    public void countsMultipleStringsOccurrencesOnList() {
        List<String> strings = Arrays.asList("hello", "world", "hello", "world", "again", "hello");

        Map<String, Integer> results = frequencyCounter.count(strings);

        assertThat(results, aMapWithSize(3));
        assertThat(results.get("hello"), is(3));
        assertThat(results.get("world"), is(2));
        assertThat(results.get("again"), is(1));
    }
}