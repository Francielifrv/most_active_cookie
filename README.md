# Most active cookie

This application runs through a CSV file and and prints the most frequent cookies based a specific date.

#### Dependencies
* Java 11
* [Gradle](https://gradle.org/)

#### Running the application

1. Generate the executable jar file
`gradle jar`

2. Run the bash command
`sh most_active_cookie.sh file_path date`

#### Running tests

`gradle test` or
`./gradlew test` (if you don't have gradle installed you may run gradlew script instead. It may take some time when running for the first time.)
